from flask import Flask, request, render_template, redirect,url_for, flash
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'flaskcontact'
mysql = MySQL(app)

app.secret_key = 'mysecretkey'

@app.route('/')
def Index():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM contacts')
    data = cur.fetchall()
    return render_template('index.html', contacts = data)

@app.route('/add_contact', methods=['POST'])
def add_contact():
    if request.method == 'POST':
        fullname = request.form['fullname']
        text = request.form['text']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO contacts(fullname, text) VALUES(%s, %s)', (fullname, text))
        mysql.connection.commit()
        return redirect(url_for('Index'))

@app.route('/about')
def about():
    return render_template('about.html')

if __name__=='__main__':
    app.run(port = 3000, debug = True)
